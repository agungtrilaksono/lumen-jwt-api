<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TestUser extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDatabase()
    {
        $users = factory(App\User::class, 3)->create();
    }

    public function testPost()
    {
        $post = factory(App\Post::class, 4)->create();
    }

    public function testComment()
    {
        $comment = factory(App\Comment::class, 4)->create();
    }

    public function testroles()
    {
        $role = factory(App\Roles::class, 4)->create();      
    }
}
