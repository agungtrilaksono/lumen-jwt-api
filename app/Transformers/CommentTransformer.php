<?php

namespace App\Transformers;

use App\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
	/**
	 * this for include reply for comment
	 * @var array
	 * @return Illuminate\Database\Eloquent\Model;
	 */
	protected $defaultIncludes = [
		'replies'
	];

	/**
	 * The transform
	 * @return App\Comment;
	 * @return Illuminate\Database\Eloquent\Model;
	 */
	public function transform(Comment $comment)
	{

		return [
			'id'		=> (int) $comment->id,
			'name'		=> $comment->name,
			'email'		=> $comment->email,
			'content'	=> $comment->content,
		];
	}

	/**
	 * Include Replies
	 * @return Illuminate\Database\Eloquent\Model;
	 */
	public function includeReplies(Comment $comment)
	{
		$replies = $comment->replies;

		return $this->collection($replies, new ReplyTransformer);
	}
}