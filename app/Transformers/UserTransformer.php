<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
	/**
	 * The transform
	 * @return App\Comment;
	 * @return Illuminate\Database\Eloquent\Model;
	 */
	public function transform(User $user)
	{
		return [
			'id'	=> (int) $user->id,
			'name'	=> $user->name,
			'email'	=> $user->email,
		];
	}
}