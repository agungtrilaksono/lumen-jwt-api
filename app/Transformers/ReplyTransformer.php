<?php

namespace App\Transformers;

use App\Replies;
use League\Fractal\TransformerAbstract;

class ReplyTransformer extends TransformerAbstract
{
	protected $defautlIncludes = [
		'users'
	];
	
	public function transform(Replies $reply)
	{
		return [
			'id'		=> (int) $reply->id,
			'content'	=> $reply->content,
			'slug'		=> $reply->slug,
		];
	}

	public function includeUsers(Replies $reply)
	{
		$reply = $reply->users;

		return $this->collection($reply, new UserTransformer);
	}
}