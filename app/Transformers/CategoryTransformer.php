<?php

namespace App\Transformers;

use App\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
	public function transform(Category $categories)
	{
		return [
			'id'	=> (int) $categories->id,
			'name'	=> $categories->name,
			'slug'	=> $categories->slug,
		];
	}
}