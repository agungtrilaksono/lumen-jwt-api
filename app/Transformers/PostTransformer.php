<?php

namespace App\Transformers;

use App\Post;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
	/**
	 * add default include
	 * @var array
	 */
	protected $defaultIncludes = [
		'author',
		'categories'
	];

	/**
	 * transfromer
	 * @return array
	 * @return App\Post
	 */
	public function transform(Post $post)
	{
		return [
			'id'			=> (int) $post->id,
			'title'			=> $post->title,
			'slug'			=> $post->slug,
			'image'			=> $post->image,
			'is_published'	=> $post->is_published,
		];
	}

	/**
	 * include a categories
	 * @return Illuminate\Database\Eloquent\Model;
	 * @return App\Category
	 */
	public function includeCategories(Post $post)
	{
		$categories = $post->categories;

		return $this->collection($categories, new CategoryTransformer);
	}

	/**
	 * include author
	 * @return Illuminate\Database\Eloquent\Model;
	 * @return App\User;
	 */
	public function includeAuthor(Post $post)
	{
		$author = $post->author;

		return $this->collection($author, new UserTransformer);
	}
}