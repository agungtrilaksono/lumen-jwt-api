<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Laravel\lumen\Application;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Application $app)
    {
        return new JsonResponse(['message' => $app->version()]);
    }
}
